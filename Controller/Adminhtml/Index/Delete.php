<?php

namespace Sanipex\Brochures\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Sanipex\Brochures\Model\BrochureFactory;

class Delete extends Action {

    const ADMIN_RESOURCE = 'Index';

    protected $resultPageFactory;
    protected $brochureFactory;

    public function __construct(
    Context $context, PageFactory $resultPageFactory, BrochureFactory $brochureFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->brochureFactory = $brochureFactory;
        parent::__construct($context);
    }

    public function execute() {
        $id = $this->getRequest()->getParam('id');

        $brochure = $this->brochureFactory->create()->load($id);

        if (!$brochure) {
            $this->messageManager->addError(__('Unable to process. please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/', array('_current' => true));
        }

        try {
            $brochure->delete();
            $this->messageManager->addSuccess(__('Your brochure has been deleted !'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Error while trying to delete brochure'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }

}
