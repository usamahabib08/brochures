<?php

namespace Sanipex\Brochures\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Brochure extends AbstractDb {

    protected function _construct() {
        $this->_init('sanipex_brochures_content', 'id');
    }

    protected function saveStore($object) {
        $condition = $this->getConnection()->quoteInto('brochure_id = ?', $object->getId());
        $this->getConnection()->delete($this->getTable('sanipex_brochures_content_store'), $condition);
        foreach ((array) $object->getData('store_id') as $store) {
            $storeArray = [
                'brochure_id' => $object->getId(),
                'store_id' => $store,
                'position' => $object->getPosition(),
            ];
            $this->getConnection()->insert(
                    $this->getTable('sanipex_brochures_content_store'), $storeArray
            );
        }
    }

    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object) {
        if (!$object->getIsMassStatus()) {
            $this->saveStore($object);
        }

        return parent::_afterSave($object);
    }

    public function loadStore(\Magento\Framework\Model\AbstractModel $object) {

        $select = $this->getConnection()->select()
                ->from($this->getTable('sanipex_brochures_content_store'))
                ->where('brochure_id = ?', $object->getId());

        if ($data = $this->getConnection()->fetchAll($select)) {
            $array = [];
            foreach ($data as $row) {
                $array[] = $row['store_id'];
                $position = $row['position'];
            }
            $object->setData('store_id', $array);
            $object->setData('position', $position);
        }

        return $object;
    }

    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object) {
        if (!$object->getIsMassDelete()) {
            $this->loadStore($object);
        }

        return parent::_afterLoad($object);
    }

}
