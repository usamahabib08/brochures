<?php

namespace Sanipex\Brochures\Model\ResourceModel\Brochure;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected $_idFieldName = 'id';

    protected function _construct() {
        $this->_init('Sanipex\Brochures\Model\Brochure', 'Sanipex\Brochures\Model\ResourceModel\Brochure');
    }

    protected function _initSelect() {
        parent::_initSelect();
        $this->getSelect()->join(['u' => $this->getTable('sanipex_brochures_content_store')], 'u.brochure_id = main_table.id', ['u.store_id','u.position'])->group('brochure_id');
    }

}
