<?php

namespace Sanipex\Brochures\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('sanipex_brochures_content');
        if ($installer->getConnection()->isTableExists($tableName) != true) {

            $table = $installer->getConnection()
                    ->newTable($tableName)
                    ->addColumn(
                            'id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                            ], 'ID'
                    )
                    ->addColumn(
                            'title', Table::TYPE_TEXT, null, ['nullable' => false, 'default' => ''], 'Title'
                    )
                    ->addColumn(
                            'description', Table::TYPE_TEXT, null, ['nullable' => false, 'default' => ''], 'Description'
                    )
                    ->addColumn(
                            'link', Table::TYPE_TEXT, null, ['nullable' => false, 'default' => ''], 'Brochure Link'
                    )
                    ->addColumn(
                            'main-image', Table::TYPE_TEXT, null, ['nullable' => false, 'default' => ''], 'Brochure image'
                    )
                    ->addColumn(
                            'created_at', Table::TYPE_DATETIME, null, ['nullable' => false], 'Created At'
                    )
                    ->addColumn(
                            'status', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '0'], 'Status'
                    )
                    ->setComment('Brochures Table')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }


        $tableName2 = $installer->getTable('sanipex_brochures_content_store');
        if ($installer->getConnection()->isTableExists($tableName2) != true) {

            $table = $installer->getConnection()
                    ->newTable($tableName2)
                    ->addColumn(
                            'brochure_id', Table::TYPE_INTEGER, null, [
                        'nullable' => false,
                            ], 'ID'
                    )
                    ->addColumn(
                            'store', Table::TYPE_INTEGER, null, [
                        'default' => 0,
                        'nullable' => false,
                            ], 'Store'
                    )
                    ->addColumn(
                            'position', Table::TYPE_INTEGER, null, [
                        'default' => 0,
                        'nullable' => false,
                            ], 'Position'
                    )
                    ->setComment('Brochures Table Store Mapping')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }

}
