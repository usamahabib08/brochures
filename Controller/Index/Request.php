<?php

namespace Sanipex\Brochures\Controller\Index;


use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;


class Request extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface {

    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        return null;
    }


    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    public function execute() {

        $post = (array) $this->getRequest()->getPost();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $storeID = $storeManager->getStore()->getStoreId();
        $storeCode = $storeManager->getStore()->getCode();
        $noKeyStores = [2, 3, 4, 5, 6];
        if (!empty($post)) {


            $name = $post['name'];
            $occupation = $post['occupation'];
            $email = $post['email'];
            $telephone = $post['telephone'];
            $address = $post['address'];
            $brochures = [];
            $taggedBrochures = [];
            
            if(isset($post['brochure'])){
                $brochures = $post['brochure'];
            }
            
            foreach($brochures as $brochure){
                $taggedBrochures[] = array('name'=> $brochure);
            }
            
            $receiverInfo = [
                'name' => 'Reciver Name',
                'email' => 'usama@sanipexgroup.com'
            ];
            
            $senderInfo = [
                'name' => 'Sender Name',
                'email' => 'info@bagnodesignlondon.com'
            ];
            $emailTempVariables = array();
            $emailTempVariables['name'] = $name;
            $emailTempVariables['occupation'] = $occupation;
            $emailTempVariables['email'] = $email;
            $emailTempVariables['telephone'] = $telephone;
            $emailTempVariables['address'] = $address;
            $emailTempVariables['brochures'] = $taggedBrochures;

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $objectManager->get('Sanipex\Brochures\Helper\Email')->yourCustomMailSendMethod(
                    $emailTempVariables, $senderInfo, $receiverInfo
            );
            
            $this->messageManager->addSuccessMessage('Brochure Requested !');
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

            if (!in_array($storeID, $noKeyStores))
                $resultRedirect->setUrl('/' . $storeCode . '/brochures/');
            else
                $resultRedirect->setUrl('/brochures/');

            return $resultRedirect;
        }

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

}
