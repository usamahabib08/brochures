<?php

namespace Sanipex\Brochures\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action {

    const ADMIN_RESOURCE = 'Sanipex_Brochures::manage_brochures';

    protected $resultPageFactory;

    public function __construct(
    Context $context, PageFactory $resultPageFactory) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute() {
        return $this->resultPageFactory->create();
    }

}
