<?php

namespace Sanipex\Brochures\Block;

use Magento\Framework\View\Element\Template;
use Sanipex\Brochures\Model\BrochureFactory;
use Magento\Framework\UrlInterface;

class Index extends \Magento\Framework\View\Element\Template {

    protected $_brochuresFactory;
    protected $_resource;
    public $_storeManager;

    public function __construct(
    Template\Context $context, BrochureFactory $brochuresFactory, \Magento\Framework\App\ResourceConnection $Resource, \Magento\Store\Model\StoreManagerInterface $storeManager, array $data = []
    ) {
        $this->_brochuresFactory = $brochuresFactory;
        $this->_resource = $Resource;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    protected function _construct() {
        parent::_construct();
        $storeID = $this->_storeManager->getStore()->getStoreId();
        $collection = $this->_brochuresFactory->create()->getCollection()->addFieldToFilter('store_id', array(
                    array('finset' => array('0')),
                    array('finset' => array($storeID)),
                        )
                )
                ->setOrder('position', 'DESC');

        
//        $joinConditions = 'u.brochure_id = main_table.id AND u.store_id IN( ' . $storeID . ',0)';
//        $collection->getSelect()->join(
//                ['u' => $collection->getTable('sanipex_brochures_content_store')], $joinConditions, ['u.store_id', "u.position"]
//        )->group('brochure_id');
//        $collection->setOrder('position', 'DESC');
        $this->setCollection($collection);
    }

}
