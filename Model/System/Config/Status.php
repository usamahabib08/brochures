<?php

namespace Sanipex\Brochures\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

class Status implements ArrayInterface {

    const ENABLED = 1;
    const DISABLED = 0;

    public static function getOptionArray() {
        return [self::ENABLED => __('Enabled'), self::DISABLED => __('Disabled')];
    }

    public function toOptionArray() {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    public function getAllOptions() {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    public function getOptionText($optionId) {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }

}
