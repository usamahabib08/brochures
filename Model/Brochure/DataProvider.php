<?php

namespace Sanipex\Brochures\Model\Brochure;

use Sanipex\Brochures\Model\ResourceModel\Brochure\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Store\Model\StoreManagerInterface;

class DataProvider extends AbstractDataProvider {

    protected $collection;
    protected $_loadedData;

    public function __construct(
    $name, $primaryFieldName, $requestFieldName, CollectionFactory $brouchureCollectionFactory, StoreManagerInterface $storeManager, array $meta = [], array $data = []
    ) {
        $this->collection = $brouchureCollectionFactory->create();
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData() {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $items = $this->collection->getItems();
        foreach ($items as $brouchure) {
            $_data = $brouchure->getData();
            $brouchure->load($brouchure->getId());
            if (isset($_data['main-image'])) {
                $image = [];
                $image[0]['name'] = $brouchure->getData('main-image');
                $image[0]['url'] = $this->getMediaUrl() . $brouchure->getData('main-image');
                $_data['main-image'] = $image;
            }
            
            if (isset($_data['store_id'])) {
                $_data['store'] = $this->getStoreIds($brouchure->getId());
            }
            if (isset($_data['position'])) {
                $_data['position'] = $this->getPosition($brouchure->getId());
            }
            $brouchure->setData($_data);
            $this->_loadedData[$brouchure->getId()] = $_data;
        }
        return $this->_loadedData;
    }

    private function getStoreIds($brochureId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "Select store_id from sanipex_brochures_content_store where brochure_id  = $brochureId";
        $data = $connection->fetchAll($query);
        $return = [];
        foreach ($data as $row) {
            $return[] = $row['store_id'];
        }
        return $return;
    }

    private function getPosition($brochureId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "Select distinct position from sanipex_brochures_content_store where brochure_id  = $brochureId";
        $data = $connection->fetchAll($query);
        $return = $data[0]['position'];

        return $return;
    }

    public function getMediaUrl() {
        $mediaUrl = $this->storeManager->getStore()
                        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'brochuresimages/tmp/brochures/';
        return $mediaUrl;
    }

}
