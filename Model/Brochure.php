<?php

namespace Sanipex\Brochures\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Brochure extends AbstractModel implements IdentityInterface {

    const CACHE_TAG = 'sanipex_brochures_content';

    protected function _construct() {
        $this->_init('Sanipex\Brochures\Model\ResourceModel\Brochure');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
