<?php

namespace Sanipex\Brochures\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $connection = $setup->getConnection();
            $connection->dropTable($connection->getTableName('sanipex_brochures_content_store'));
            $tableName2 = $setup->getTable('sanipex_brochures_content_store');

            if ($setup->getConnection()->isTableExists($tableName2) != true) {

                $table = $setup->getConnection()
                        ->newTable($tableName2)
                        ->addColumn(
                                'id', Table::TYPE_INTEGER, null, [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true
                                ], 'ID'
                        )
                        ->addColumn(
                                'brochure_id', Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Brochure Id'
                        )
                        ->addColumn(
                                'store_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Store Id'
                        )
                        ->addColumn(
                                'position', Table::TYPE_INTEGER, null, [
                            'default' => 0,
                            'nullable' => false,
                                ], 'Position'
                        )->addForeignKey(
                                $setup->getFkName(
                                        'sanipex_brochures_content_store', 'store_id', 'store', 'store_id'
                                ), 'store_id', $setup->getTable('store'), 'store_id', Table::ACTION_CASCADE
                        )
                        ->addForeignKey(
                                $setup->getFkName(
                                        'sanipex_brochures_content_store', 'brochure_id', 'sanipex_brochures_content', 'id'
                                ), 'brochure_id', $setup->getTable('sanipex_brochures_content'), 'id', Table::ACTION_CASCADE
                        )
                        ->setComment('Brochures Table Store Mapping')
                        ->setOption('type', 'InnoDB')
                        ->setOption('charset', 'utf8');
                $setup->getConnection()->createTable($table);
            }
        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $setup->getConnection()->changeColumn(
                    $setup->getTable('sanipex_brochures_content_store'), 'id', 'link_id', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'primary' => true,
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,        
                'comment' => 'Full name of customer'
                    ]
            );
        }
        $setup->endSetup();
    }

}
