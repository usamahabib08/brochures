<?php

namespace Sanipex\Brochures\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Sanipex\Brochures\Model\BrochureFactory;

class Save extends Action {

    const ADMIN_RESOURCE = 'Index';

    protected $resultPageFactory;
    protected $brochureFactory;

    public function __construct(
    Context $context, PageFactory $resultPageFactory, BrochureFactory $brochureFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->brochureFactory = $brochureFactory;
        parent::__construct($context);
    }

    public function execute() {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $data = $this->_filterMainImagesData($data);

            try {
                $id = $data['id'];

                $brochure = $this->brochureFactory->create()->load($id);

                $data = array_filter($data, function($value) {
                    return $value !== '';
                });

                $brochure->setData($data);
                $brochure->save();
                $this->messageManager->addSuccess(__('Successfully saved the item.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $brochure->getId()]);
            }
        }

        return $resultRedirect->setPath('*/*/');
    }

    public function _filterMainImagesData(array $rawData) {
        $data = $rawData;

        if (isset($data['main-image'])) {
            $data = $rawData;
            if (isset($data['main-image'][0]['name'])) {
                $data['main-image'] = $data['main-image'][0]['name'];
            } else {
                $data['main-image'] = null;
            }
            return $data;
        }
    }
    
}