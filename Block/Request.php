<?php

namespace Sanipex\Brochures\Block;

use Sanipex\Brochures\Model\BrochureFactory;

class Request extends \Magento\Framework\View\Element\Template {

    protected $_brochuresFactory;

    public function __construct(
    \Magento\Backend\Block\Template\Context $context, BrochureFactory $brochuresFactory, array $data = []
    ) {
        $this->_brochuresFactory = $brochuresFactory;
        parent::__construct($context, $data);
    }

    public function getBrochuresList() {
        $collection = $this->_brochuresFactory->create()->getCollection();

        return $collection;
    }

    public function getFormAction() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $storeCode = $storeManager->getStore()->getCode();
        $storeID = $storeManager->getStore()->getStoreId();
        $noKeyStores = [2, 3, 4, 5, 6];
        if (!in_array($storeID, $noKeyStores))
            return '/' . $storeCode . '/brochures/index/request';
        else
            return '/brochures/index/request';
    }

}
